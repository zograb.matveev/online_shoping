package am.matveev.onlineshop.OnlineShop.security;

import am.matveev.onlineshop.OnlineShop.models.PersonEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;


public class PersonDetails implements UserDetails{

    private final PersonEntity person;

    @Autowired
    public PersonDetails(PersonEntity person){
        this.person = person;
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities(){
        return Collections.singletonList(new SimpleGrantedAuthority(person.getRole()));
    }

    @Override
    public String getPassword(){
        return this.person.getPassword();
    }

    @Override
    public String getUsername(){
        return this.person.getName();
    }

    @Override
    public boolean isAccountNonExpired(){
        return true;
    }

    @Override
    public boolean isAccountNonLocked(){
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired(){
        return true;
    }

    @Override
    public boolean isEnabled(){
        return true;
    }

    public PersonEntity getPerson(){
        return this.person;
    }
}
