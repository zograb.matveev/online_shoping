package am.matveev.onlineshop.OnlineShop.controllers;

import am.matveev.onlineshop.OnlineShop.dto.OrderDTO;
import am.matveev.onlineshop.OnlineShop.services.OrderService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/orders")
@Validated
public class OrderController{

    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService){
        this.orderService = orderService;
    }

    @GetMapping()
    public List<OrderDTO> findAll(){
        return orderService.findAll();
    }

    @GetMapping("/{id}")
    public OrderDTO findOne(@PathVariable long id){
        return orderService.findOne(id);
    }

    @PostMapping("/registration")
    public ResponseEntity<HttpStatus> create(@RequestBody @Valid OrderDTO orderDTO){
       orderService.create(orderDTO);
       return ResponseEntity.ok(HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<HttpStatus> update(@PathVariable long id,@RequestBody OrderDTO orderDTO){
        orderService.update(id,orderDTO);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable long id){
        orderService.delete(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<HttpStatus> update(@PathVariable long id,Integer quantity){
        orderService.update(id,quantity);
        return ResponseEntity.ok(HttpStatus.OK);
    }

}
