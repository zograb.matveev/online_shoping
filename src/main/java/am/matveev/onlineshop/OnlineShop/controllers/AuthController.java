package am.matveev.onlineshop.OnlineShop.controllers;

import am.matveev.onlineshop.OnlineShop.dto.AuthenticationDTO;
import am.matveev.onlineshop.OnlineShop.dto.PersonDTO;
import am.matveev.onlineshop.OnlineShop.dto.TokenResponse;
import am.matveev.onlineshop.OnlineShop.security.JWTUtil;
import am.matveev.onlineshop.OnlineShop.security.PersonDetails;
import am.matveev.onlineshop.OnlineShop.services.PersonService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/auth")
public class AuthController{

    private final PersonService personService;
    private final JWTUtil jwtUtil;
    private final AuthenticationManager authenticationManager;

    @Autowired
    public AuthController(PersonService personService, JWTUtil jwtUtil, AuthenticationManager authenticationManager){
        this.personService = personService;
        this.jwtUtil = jwtUtil;
        this.authenticationManager = authenticationManager;
    }

    @PostMapping("/login")
    public ResponseEntity<String> loginPage(@RequestBody AuthenticationDTO authenticationDTO){
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(authenticationDTO.getName(),
                        authenticationDTO.getPassword());

        try{
            authenticationManager.authenticate(authenticationToken);
        }catch(BadCredentialsException e){
            return ResponseEntity.ok("Incorrect login or password");
        }
        String token = jwtUtil.generateToken(authenticationDTO.getName());
        return ResponseEntity.ok("You have successfully logged in!");
    }

    @PostMapping("/registration")
    public ResponseEntity<TokenResponse> performRegistration(@RequestBody @Valid PersonDTO personDTO){
        TokenResponse tokenResponse = personService.register(personDTO);
        return ResponseEntity.ok(tokenResponse);
    }

    @GetMapping("/showUser")
    @ResponseBody
    public String showUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        PersonDetails personDetails = (PersonDetails)authentication.getPrincipal();

        return personDetails.getUsername();
    }

}
