package am.matveev.onlineshop.OnlineShop.controllers;

import am.matveev.onlineshop.OnlineShop.dto.PersonDTO;
import am.matveev.onlineshop.OnlineShop.dto.PersonWithOrdersDTO;
import am.matveev.onlineshop.OnlineShop.services.PersonService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/person")
public class PersonController{

    private final PersonService personService;


    @Autowired
    public PersonController(PersonService personService){
        this.personService = personService;
    }

    @GetMapping()
    public List<PersonDTO> findAll(){
        return personService.findAll();
    }

    @GetMapping("/{id}")
    public PersonDTO findOne(@PathVariable long id){
        return personService.findOne(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<HttpStatus> updatePerson(@PathVariable long id,@RequestBody @Valid PersonDTO personDTO){
        personService.update(personDTO, id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deletePerson(@PathVariable long id){
        personService.delete(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

//    @GetMapping("/orders")
//    public ResponseEntity<List<PersonWithOrdersDTO>> getAllPersonsWithOrders() {
//        List<PersonWithOrdersDTO> personWithOrders = personService.getAllPersonsWithOrders();
//        return ResponseEntity.ok(personWithOrders);
//    }
//
//    @GetMapping("/{id}/orders")
//    public ResponseEntity<PersonWithOrdersDTO> getOrderByPersonId(@PathVariable long id){
//        PersonWithOrdersDTO person = personService.getOrderByPersonId(id);
//        return ResponseEntity.ok(person);
//    }
}
