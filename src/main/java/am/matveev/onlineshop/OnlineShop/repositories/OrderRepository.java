package am.matveev.onlineshop.OnlineShop.repositories;

import am.matveev.onlineshop.OnlineShop.models.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<OrderEntity,Long>{
    Optional<OrderEntity> findByProduct(String product);
}
