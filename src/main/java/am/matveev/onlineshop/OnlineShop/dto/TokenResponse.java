package am.matveev.onlineshop.OnlineShop.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TokenResponse {

    private String token;
}
