package am.matveev.onlineshop.OnlineShop.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PersonWithOrdersDTO{

    private Long id;
    private String name;
    private List<OrderDTO> orders;

}
