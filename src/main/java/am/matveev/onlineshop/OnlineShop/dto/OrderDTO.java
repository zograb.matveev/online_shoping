package am.matveev.onlineshop.OnlineShop.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;


@Getter
@Setter
public class OrderDTO{

    @NotEmpty(message = "Product should not be empty")
    private String product;

    @NotEmpty(message = "Description should not be empty")
    private String description;

    @NotNull
    private Double price;

    @NotNull(message = "Quantity shouldn't be null")
    @Positive(message = "Specify number more than 1")
    private Integer quantity;


    //if you want to show also Person fileds you should use Persing DTO
    //just
    //private PersonDTO person;
    //but since you are using current DTO (OrderDTO) for order creation
    // I suggest you to write new Order dto for example OrderResponseDTO
    // And there add above 3 fileds and PersonDTO too
    // but it could be better if you create also PersonResponseDTO too
    // for that DTO-s in mapper services you can use toResponseDTO(SomeEntity);
    //have a good day, I love you!!! You can do it!!!

}
