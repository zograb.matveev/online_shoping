package am.matveev.onlineshop.OnlineShop.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class GlobalExceptionHandler{


    @ExceptionHandler({PersonNotFoundException.class})
    public ResponseEntity<ErrorResponse> handleException(PersonNotFoundException e){
        ErrorResponse response = new ErrorResponse(
                "Person with this id wasn't found",
                LocalDateTime.now()
        );
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({PersonNotUpdatedException.class})
    public ResponseEntity<ErrorResponse> handleException(PersonNotUpdatedException e){
        ErrorResponse response = new ErrorResponse(
                "Person with this id wasn't update",
                LocalDateTime.now()
        );
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({ProductNotFoundException.class})
    public ResponseEntity<ErrorResponse> handleException(ProductNotFoundException e){
        ErrorResponse response = new ErrorResponse(
                "Product with this id wasn't found",
                LocalDateTime.now()
        );
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({EmailAlreadyExistException.class})
    public ResponseEntity<ErrorResponse> handleException(EmailAlreadyExistException e){
        ErrorResponse response = new ErrorResponse(
                "Person with this email already exist!",
                LocalDateTime.now()
        );
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ProductNotCreatedException.class})
    public ResponseEntity<ErrorResponse> handleException(ProductNotCreatedException e){
        ErrorResponse response = new ErrorResponse(
                "Product with this name already exist!",
                LocalDateTime.now()
        );
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ProductNotUpdateException.class})
    public ResponseEntity<ErrorResponse> handleException(ProductNotUpdateException e){
        ErrorResponse response = new ErrorResponse(
                "Product with this id not found!",
                LocalDateTime.now()
        );
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({PersonNotRegisteredException.class})
    public ResponseEntity<ErrorResponse> handleException(PersonNotRegisteredException e){
        ErrorResponse response = new ErrorResponse(
                "Person not registration!",
                LocalDateTime.now()
        );
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<ErrorResponse> handleException(MethodArgumentNotValidException e){
        ErrorResponse response = new ErrorResponse(
                e.getMessage(),
                LocalDateTime.now()
        );
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
