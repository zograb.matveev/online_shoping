package am.matveev.onlineshop.OnlineShop.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "Person")
@Getter
@Setter
public class PersonEntity{


    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "email",unique = true)
    private String email;

    @Column(name = "password")
    private String password;

    @ManyToMany(mappedBy = "person",fetch = FetchType.LAZY)
    private List<OrderEntity> orders;

    @Column(name = "role")
    private String role;


}
