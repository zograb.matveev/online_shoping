package am.matveev.onlineshop.OnlineShop.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "Orders")
@Getter
@Setter
public class OrderEntity{

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "product")
    private String product;

    @Column(name = "description")
    private String description;

    @Column(name = "price")
    private double price;

    @Column(name = "quantity")
    private Integer quantity;


    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "Person_Orders",
            joinColumns = @JoinColumn(name = "orders_id"),
            inverseJoinColumns = @JoinColumn(name = "person_id")
    )
    private List<PersonEntity> person;



}
