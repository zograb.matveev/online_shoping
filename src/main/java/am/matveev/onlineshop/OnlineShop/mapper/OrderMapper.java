package am.matveev.onlineshop.OnlineShop.mapper;

import am.matveev.onlineshop.OnlineShop.dto.OrderDTO;
import am.matveev.onlineshop.OnlineShop.models.OrderEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OrderMapper{

    OrderDTO toDto(OrderEntity orders);
    OrderEntity toEntity(OrderDTO orderDTO);
}
