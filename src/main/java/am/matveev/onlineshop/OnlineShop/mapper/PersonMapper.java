package am.matveev.onlineshop.OnlineShop.mapper;

import am.matveev.onlineshop.OnlineShop.dto.PersonDTO;
import am.matveev.onlineshop.OnlineShop.dto.PersonWithOrdersDTO;
import am.matveev.onlineshop.OnlineShop.models.PersonEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


@Mapper(componentModel = "spring",uses = OrderMapper.class)
public interface PersonMapper{

    PersonDTO toDTO(PersonEntity personEntity);
    PersonEntity toEntity(PersonDTO personDTO);

    @Mapping(source = "id", target = "id")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "orders", target = "orders")
    PersonWithOrdersDTO toPersonWithOrderDTO(PersonEntity person);

}




