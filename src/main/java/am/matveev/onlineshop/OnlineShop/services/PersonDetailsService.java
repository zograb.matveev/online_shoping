package am.matveev.onlineshop.OnlineShop.services;

import am.matveev.onlineshop.OnlineShop.models.PersonEntity;
import am.matveev.onlineshop.OnlineShop.repositories.PersonRepository;
import am.matveev.onlineshop.OnlineShop.security.PersonDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PersonDetailsService implements UserDetailsService{

    private final PersonRepository personRepository;

    @Autowired
    public PersonDetailsService(PersonRepository personRepository){
        this.personRepository = personRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException{
        Optional<PersonEntity> person = personRepository.findByName(name);
        if(person.isEmpty()){
            throw new UsernameNotFoundException("User not found");
        }
        return new PersonDetails(person.get());
    }

}

