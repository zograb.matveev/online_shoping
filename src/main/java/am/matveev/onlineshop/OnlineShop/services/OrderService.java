package am.matveev.onlineshop.OnlineShop.services;

import am.matveev.onlineshop.OnlineShop.dto.OrderDTO;
import am.matveev.onlineshop.OnlineShop.exception.ProductNotFoundException;
import am.matveev.onlineshop.OnlineShop.exception.ProductNotUpdateException;
import am.matveev.onlineshop.OnlineShop.mapper.OrderMapper;
import am.matveev.onlineshop.OnlineShop.models.OrderEntity;
import am.matveev.onlineshop.OnlineShop.repositories.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class OrderService{

    private final OrderRepository orderRepository;
    private final OrderMapper orderMapper;

    @Autowired
    public OrderService(OrderRepository orderRepository, OrderMapper orderMapper){
        this.orderRepository = orderRepository;
        this.orderMapper = orderMapper;
    }

    @Transactional(readOnly = true)
    public List<OrderDTO> findAll(){
        List<OrderEntity> order = orderRepository.findAll();
        List<OrderDTO> orderDTOS = order.stream()
                .map(orderMapper :: toDto)
                .collect(Collectors.toList());
        return orderDTOS;
    }

    @Transactional(readOnly = true)
    public OrderDTO findOne(long id){
        OrderEntity order = orderRepository.findById(id)
                .orElseThrow(ProductNotFoundException :: new);
        return orderMapper.toDto(order);
    }

    @Transactional
    public OrderDTO create(OrderDTO orderDTO){
        OrderEntity order = orderMapper.toEntity(orderDTO);
        orderRepository.save(order);
        return orderMapper.toDto(order);
    }

    @Transactional
    public void update(long id, OrderDTO updatedOrderDTO){
        OrderEntity updateOrder = orderMapper.toEntity(updatedOrderDTO);
        if(orderRepository.findById(id).isEmpty()){
            throw new ProductNotUpdateException();
        }
        updateOrder.setId(id);
        orderRepository.save(updateOrder);
        orderMapper.toDto(updateOrder);
    }

    @Transactional
    public void delete(long id){
        orderRepository.deleteById(id);
    }

    @Transactional
    public void update(long id,Integer quantity){
        Optional<OrderEntity> order = orderRepository.findById(id);

        if(order.isEmpty()){
            throw new RuntimeException();
        }
        order.get().setQuantity(quantity);
        orderRepository.save(order.get());
    }
}
