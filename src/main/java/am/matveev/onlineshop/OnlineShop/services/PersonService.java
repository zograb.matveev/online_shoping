package am.matveev.onlineshop.OnlineShop.services;

import am.matveev.onlineshop.OnlineShop.dto.PersonDTO;
import am.matveev.onlineshop.OnlineShop.dto.PersonWithOrdersDTO;
import am.matveev.onlineshop.OnlineShop.dto.TokenResponse;
import am.matveev.onlineshop.OnlineShop.exception.EmailAlreadyExistException;
import am.matveev.onlineshop.OnlineShop.exception.PersonNotFoundException;
import am.matveev.onlineshop.OnlineShop.mapper.PersonMapper;
import am.matveev.onlineshop.OnlineShop.models.PersonEntity;
import am.matveev.onlineshop.OnlineShop.repositories.OrderRepository;
import am.matveev.onlineshop.OnlineShop.repositories.PersonRepository;
import am.matveev.onlineshop.OnlineShop.security.JWTUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class PersonService{

    private final PersonRepository personRepository;
    private final PersonMapper personMapper;
    private final PasswordEncoder passwordEncoder;
    private final OrderRepository orderRepository;
    private final JWTUtil jwtUtil;

    @Autowired
    public PersonService(PersonRepository personRepository,
                         PersonMapper personMapper,
                         PasswordEncoder passwordEncoder,
                         OrderRepository orderRepository,
                         JWTUtil jwtUtil){
        this.personRepository = personRepository;
        this.personMapper = personMapper;
        this.passwordEncoder = passwordEncoder;
        this.orderRepository = orderRepository;
        this.jwtUtil = jwtUtil;
    }

    @Transactional(readOnly = true)
    public List<PersonDTO> findAll(){
        List<PersonEntity> personEntityList = personRepository.findAll();
        List<PersonDTO> personDTOList = personEntityList.stream()
                .map(personMapper :: toDTO)
                .collect(Collectors.toList());
        return personDTOList;
    }

    @Transactional(readOnly = true)
    public PersonDTO findOne(long id){
        PersonEntity person = personRepository
                .findById(id)
                .orElseThrow(PersonNotFoundException :: new);
        return personMapper.toDTO(person);
    }

    @Transactional
    public TokenResponse register(PersonDTO personDTO){
        PersonEntity person = personMapper.toEntity(personDTO);

        if(personRepository.findByEmail(person.getEmail()).isPresent()){
            throw new EmailAlreadyExistException();
        }
        person.setPassword(passwordEncoder.encode(person.getPassword()));
        person.setRole("ROLE_USER");
        personRepository.save(person);
        String token = jwtUtil.generateToken(personDTO.getName());
        enrichedRegisteredPerson();
        TokenResponse tokenResponse = new TokenResponse();
        tokenResponse.setToken(token);
        return tokenResponse;
    }

//    @Transactional
//    public void update(PersonDTO personDTO, long id){
//        PersonEntity updatePerson = personMapper.toEntity(personDTO);
//        if(personRepository.findByEmail(updatePerson.getEmail()).isPresent()){
//            throw new EmailAlreadyExistException();
//        }
//        updatePerson.setId(id);
//        updatePerson.setPassword(passwordEncoder.encode(updatePerson.getPassword()));
//        enrichedUpdatedPerson();
//        updatePerson.setRole("ROLE_USER");
//        personRepository.save(updatePerson);
//        personMapper.toDTO(updatePerson);
//
//    }

    @Transactional
    public PersonDTO update(PersonDTO personDTO, long id) {
        PersonEntity updatePerson = personMapper.toEntity(personDTO);
        updatePerson.setId(id);
        updatePerson.setPassword(passwordEncoder.encode(updatePerson.getPassword()));
        enrichedUpdatedPerson();
        updatePerson.setRole("ROLE_USER");
        personRepository.save(updatePerson);

        return personMapper.toDTO(updatePerson);
    }

    @Transactional
    public void delete(long id){
        personRepository.deleteById(id);
    }

    public void enrichedRegisteredPerson(){
        log.info("Person was registered");
    }

    public void enrichedUpdatedPerson(){
        log.info("Person was updated");
    }


}
